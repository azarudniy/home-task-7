//
//  FormModel.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/20/21.
//

import Foundation
import Combine

class FormModel: Codable {
    var title: String?
    var image: String?
    var fields: [FieldsModel] = []
    
    func fetchData(stringURL: String) -> AnyPublisher<FormModel, Never> {
        guard let url = URL(string: stringURL) else { return Just(FormModel()).eraseToAnyPublisher() }
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: FormModel.self, decoder: JSONDecoder())
            .replaceError(with: self)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}

class FieldsModel: Codable {
    var title: String?
    var name: String?
    var type: String?
    var values: [String: String]?
}
