//
//  ViewController.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/19/21.
//

import UIKit
import SnapKit
import Combine

class ViewController: UIViewController {
    
    var arrayKeys = [String]()
    
    var formViewModel: FormViewModel = {
        let viewModel = FormViewModel()
        return viewModel
    }()
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        return table
    }()
    
    lazy var imageView: UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        return button
    }()

    //MARK: ~viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.formViewModel.formModel.fetchData(stringURL: Constants.stringURL).sink(receiveValue: { (form) in
            self.formViewModel.formModel = form
            self.title = self.formViewModel.formModel.title
            
            if let stringImageURL = self.formViewModel.formModel.image {
                self.fetchImage(stringURL: stringImageURL).sink(receiveValue: { (dataImage) in
                    DispatchQueue.main.async {
                        self.imageView.image = UIImage(data: dataImage)
                    }
                }).store(in: &self.formViewModel.observers)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }).store(in: &self.formViewModel.observers)
        
        view.backgroundColor = .white
        setUpViews()
        setUpTableView()
        
        self.formViewModel.mutableFlagCellList.sink { (value) in
            let tableViewController = TableViewController()
            tableViewController.modalPresentationStyle = .fullScreen
            tableViewController.dictValues = self.formViewModel.dictionary
            tableViewController.selectedValueDelegate = self
            self.navigationController?.pushViewController(tableViewController, animated: true)
        }.store(in: &self.formViewModel.observers)
        
        self.formViewModel.mutableTextDataInvalidFlag.sink { (value) in
            let alert = UIAlertController(title: "Wrong data!", message: "Enter valid data!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true)
            
        }.store(in: &self.formViewModel.observers)
        
    }
}

extension ViewController: SelectListValueProtocol {
    func selectValue(value: String) {
        self.arrayKeys.append(value)
    }
    
    private func setUpTableView() {
        self.view.addSubview(tableView)
        self.tableView.dataSource = formViewModel
        self.tableView.delegate = formViewModel
        tableView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview()
            $0.top.equalToSuperview()
            $0.bottom.equalTo(imageView.snp.top).offset(-5)
        }
        
        self.tableView.register(FormTableViewTextCell.self, forCellReuseIdentifier: self.formViewModel.cellTextID)
        self.tableView.register(FormTableViewNumericCell.self, forCellReuseIdentifier: self.formViewModel.cellNumericID)
        self.tableView.register(FormTableViewListCell.self, forCellReuseIdentifier: self.formViewModel.cellListID)
    }
    
    private func setUpViews() {
        view.addSubview(imageView)
        view.addSubview(saveButton)
        
        saveButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(70)
        }
        
        saveButton.setTitle("Button", for: .normal)
        saveButton.backgroundColor = .darkGray
        saveButton.addTarget(self, action: #selector(saveButtonAction), for: .touchUpInside)
        
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(saveButton.snp.top).offset(-20)
            $0.height.equalTo(100)
            $0.width.equalToSuperview().inset(10)
        }
    }
    
    private func fetchImage(stringURL: String) -> AnyPublisher<Data, Never> {
        guard let url = URL(string: stringURL) else { return Just(Data()).eraseToAnyPublisher() }
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .replaceError(with: Data())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    @objc func saveButtonAction(sender: UIButton!) {
        var cellnumber = 0
        for field in self.formViewModel.formModel.fields {
            let indexPath = IndexPath(row: cellnumber, section: 0)
            switch field.type {
            case "TEXT":
                if let cell = self.tableView.cellForRow(at: indexPath) as? FormTableViewTextCell {
                    if let value = cell.textField.text {
                        if let name = field.name {
                            self.formViewModel.dictForPost.updateValue(value, forKey: name)
                        }
                    }
                }
                cellnumber += 1
            case "NUMERIC":
                if let cell = self.tableView.cellForRow(at: indexPath) as? FormTableViewNumericCell {
                    if let value = cell.textField.text {
                        if let name = field.name {
                            self.formViewModel.dictForPost.updateValue(value, forKey: name)
                        }
                    }
                }
                cellnumber += 1
            case "LIST":
                if let name = field.name {
                    self.formViewModel.dictForPost.updateValue(arrayKeys.removeFirst(), forKey: name)
                }
                cellnumber += 1
            default:
                return
            }
        }
        for (key, value) in self.formViewModel.dictForPost {
            print("\(key): \(value)")
        }
        
        
        
        let dictForPostData = try? JSONSerialization.data(withJSONObject: ["form": self.formViewModel.dictForPost], options: .sortedKeys)
        print()
        var request: URLRequest?
        if let url = URL(string: Constants.stringPostURL) {
            request = URLRequest(url: url)
            request?.httpMethod = "POST"
        }
        request?.httpBody = dictForPostData
        
        if let requestForPublisher = request {
            URLSession.shared.dataTaskPublisher(for: requestForPublisher).sink { (subscriber) in
                switch subscriber {
                case .failure(let error):
                    print(error)
                case .finished:
                    print("Is finished")
                }
            } receiveValue: { (data: Data, response: URLResponse) in
                
                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                
                DispatchQueue.main.async {
                    if let responseJSON = responseJSON as? [String: Any] {
                        let key = Array(responseJSON.keys).first
                        let alert = UIAlertController(title: "Result", message: responseJSON[key ?? "Some error"] as? String, preferredStyle: .alert)
                        let alertOkAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(alertOkAction)
                        self.present(alert, animated: true)
                    }
                }
            }.store(in: &self.formViewModel.observers)

        }
    }
}

