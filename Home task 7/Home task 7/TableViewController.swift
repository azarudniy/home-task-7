//
//  TableViewController.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/24/21.
//

import UIKit

class TableViewController: UITableViewController {
    
    var dictValues: [String: String] = [:]
    var arrayKeys: [String] = []
    weak var selectedValueDelegate: SelectListValueProtocol?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(ListValuesCell.self, forCellReuseIdentifier: "listCellID")
        arrayKeys = Array(dictValues.keys).sorted()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictValues.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCellID", for: indexPath) as! ListValuesCell
        cell.title.text = dictValues[arrayKeys[indexPath.row]]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView.cellForRow(at: indexPath) as? ListValuesCell) != nil {
            selectedValueDelegate?.selectValue(value: arrayKeys[indexPath.row])
        }
        navigationController?.popViewController(animated: true)
    }
    
}
