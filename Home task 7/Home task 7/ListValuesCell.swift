//
//  ListValuesCell.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/25/21.
//

import UIKit

class ListValuesCell: UITableViewCell {
    
    lazy var title: UILabel = {
        let title = UILabel()
        return title
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setTitleCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setTitleCell() {
        addSubview(title)
        title.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(10)
            $0.centerY.equalToSuperview()
        }
    }

}
