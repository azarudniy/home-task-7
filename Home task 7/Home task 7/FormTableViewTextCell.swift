//
//  FormTableViewCell.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/23/21.
//

import UIKit
import SnapKit
import Combine

class FormTableViewTextCell: UITableViewCell, UITextFieldDelegate {
    
    lazy var title: UILabel = {
        let title = UILabel()
        return title
    }()
    
    lazy var textField: UITextField = {
        let field = UITextField()
        return field
    }()
    
    var item: FormTableViewProtocol = FormTableViewTextItem() {
        didSet {
            guard let item = item as? FormTableViewTextItem else { return }
            title.text = item.title
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setTitleCell()
        setTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setTitleCell() {
        addSubview(title)
        title.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(10)
            $0.centerY.equalToSuperview()
        }
    }
    
    private func setTextField() {
        addSubview(textField)
        textField.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().inset(10)
            $0.leading.equalTo(title.snp.trailing).inset(-10)
        }
        textField.text = ""
        textField.delegate = self
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
