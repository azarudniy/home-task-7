//
//  Constants.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/23/21.
//

import Foundation

enum Constants {
    static let stringURL = "http://test.clevertec.ru/tt/meta/"
    static let stringPostURL = "http://test.clevertec.ru/tt/data/"
}
