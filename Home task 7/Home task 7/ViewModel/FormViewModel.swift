//
//  FormViewModel.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/23/21.
//

import Foundation
import UIKit
import Combine

class FormViewModel: NSObject {
    var formModel: FormModel = FormModel()
    var observers: [AnyCancellable] = []
    let cellTextID = "cellTextID"
    let cellNumericID = "cellNumericID"
    let cellListID = "cellListID"
    var dictionary: [String: String] = [:]
    lazy var regex = "^[а-яА-Яa-zA-Z0-9]$"
    var dictForPost = [String: Any]()
    
    
    var isCellListTapped = false {
        didSet {
            mutableFlagCellList.send(isCellListTapped)
        }
    }
    let mutableFlagCellList = PassthroughSubject<Bool, Never>()
    
    var textDataInvalidFlag = false {
        didSet {
            mutableTextDataInvalidFlag.send(textDataInvalidFlag)
        }
    }
    let mutableTextDataInvalidFlag = PassthroughSubject<Bool, Never>()
    
}

extension FormViewModel: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formModel.fields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let field = formModel.fields[indexPath.row]
        switch field.type {
        case "TEXT":
            let cell = tableView.dequeueReusableCell(withIdentifier: cellTextID, for: indexPath) as? FormTableViewTextCell
            cell?.item.field = self.formModel.fields[indexPath.row]
            return cell ?? UITableViewCell()
        case "NUMERIC":
            let cell = tableView.dequeueReusableCell(withIdentifier: cellNumericID, for: indexPath) as? FormTableViewNumericCell
            cell?.item.field = self.formModel.fields[indexPath.row]
            return cell ?? UITableViewCell()
        case "LIST":
            let cell = tableView.dequeueReusableCell(withIdentifier: cellListID, for: indexPath) as? FormTableViewListCell
            cell?.item.field = self.formModel.fields[indexPath.row]
            return cell ?? UITableViewCell()
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.formModel.fields[indexPath.row].type {
        case "TEXT":
            if let cell = tableView.cellForRow(at: indexPath) as? FormTableViewTextCell {
                cell.textField.delegate = self
                cell.textField.becomeFirstResponder()
            }
        case "NUMERIC":
            if let cell = tableView.cellForRow(at: indexPath) as? FormTableViewNumericCell {
                cell.textField.becomeFirstResponder()
            }
        case "LIST":
            if let dict = formModel.fields[indexPath.row].values {
                dictionary = dict
            }
            isCellListTapped = !isCellListTapped
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        switch self.formModel.fields[indexPath.row].type {
        case "TEXT":
            if let cell = tableView.cellForRow(at: indexPath) as? FormTableViewTextCell {
                cell.textField.endEditing(true)
            }
        case "NUMERIC":
            if let cell = tableView.cellForRow(at: indexPath) as? FormTableViewNumericCell {
                if let stringNumber = cell.textField.text?.replacingOccurrences(of: ",", with: ".") {
                    guard let number = Double(stringNumber) else {
                        cell.textField.addTarget(self, action: #selector(editingNumberEnded(_:)), for: .editingDidEnd)
                        return
                    }
                    if number <= 1 || number >= 1024 {
                        cell.textField.addTarget(self, action: #selector(editingNumberEnded(_:)), for: .editingDidEnd)
                    }
                }
            }
        default:
            return
        }
    }
    
    @objc func editingNumberEnded(_ textField: UITextField) {
        Thread.sleep(until: Date()+0.500)
        textDataInvalidFlag = !textDataInvalidFlag
        textField.text = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text ?? "") + string
        let res: String
        if range.length == 1 {
            let end = text.index(text.startIndex, offsetBy: text.count - 1)
            res = String(text[text.startIndex..<end])
        } else {
            res = text
        }
        
        if checkText(text: res) {
            textField.text = ""
        }
        textField.text = res
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func checkText(text: String) -> Bool{
        guard text.matches(regex) else {
            textDataInvalidFlag = !textDataInvalidFlag
            return true
        }
        return false
    }
    
}

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
