//
//  FormTableViewItemType.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/23/21.
//

import Foundation

enum FormTableViewItemType {
    case text
    case numeric
    case list
}

protocol FormTableViewProtocol {
    var type: FormTableViewItemType { get }
    var title: String { get }
    var field: FieldsModel? { set get }
}

extension FormTableViewProtocol {
    var title: String {
        return field?.title ?? ""
    }
}

class FormTableViewTextItem: FormTableViewProtocol {
    var field: FieldsModel?
    var type: FormTableViewItemType {
        return .text
    }
}

class FormTableViewNumericItem: FormTableViewProtocol {
    var type: FormTableViewItemType {
        return .numeric
    }
    var field: FieldsModel?
}

class FormTableViewListItem: FormTableViewProtocol {
    var type: FormTableViewItemType {
        return .list
    }
    var field: FieldsModel?
    
    var itemsNumber: Int {
        return field?.values?.count ?? 0
    }
}
