//
//  FormTableViewListCell.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/24/21.
//

import UIKit

class FormTableViewListCell: UITableViewCell {
    
    lazy var title: UILabel = {
        let title = UILabel()
        return title
    }()
    
    var item: FormTableViewProtocol = FormTableViewListItem() {
        didSet {
            guard let item = item as? FormTableViewListItem else { return }
            title.text = item.title
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setTitleCell()
        self.accessoryType = .disclosureIndicator
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setTitleCell() {
        addSubview(title)
        title.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(10)
            $0.centerY.equalToSuperview()
        }
    }

}
