//
//  SelectListValueProtocol.swift
//  Home task 7
//
//  Created by Александр Зарудний on 10/25/21.
//

import Foundation

protocol SelectListValueProtocol: class {
    func selectValue(value: String)
}
